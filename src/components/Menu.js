import React, { Component } from 'react';
import { Text, View, Image,  StyleSheet } from 'react-native';

import Button from './Button';
import { Actions } from 'react-native-router-flux';


const Menu = () => {
  return (
    <View style={{flex:0.7, flexDirection: 'column'}}>
      <View style={styles.title} >
        <Text style={{fontSize: 30, fontWeight: 'bold'}}>¡Bienvenidos!</Text>
        <Text style={{fontSize: 20, fontWeight: 'bold'}}>Cliente de Flickr</Text>
      </View>
      <View style={{flex: 2, marginBottom:10, flexDirection: 'row'}}>
        <Button  onPress={() => Actions.explorer()}>
          Explorar
        </Button>
        <Button  onPress={() => Actions.albumList()}>
          Ver álbumes
        </Button>
      </View>
      <View style={{flex: 1, alignItems: 'center'}}>
        <Text style={{fontSize: 20}}>UTN - FRC</Text>
        <Text style={{fontSize: 18}}>Benito, Federico - Filardo, Juan Ignacio</Text>
      </View>
    </View>
  );
};

var styles = StyleSheet.create({
  title: {
    flex: 1,
    alignItems: 'center',
    marginRight: 28
  }
});

export default Menu;
