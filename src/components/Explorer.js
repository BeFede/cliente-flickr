
import React, { Component } from 'react';
import { ScrollView, Text, View, TextInput, Button } from 'react-native';
import axios from 'axios';
import PhotoDetail from './PhotoDetail';
import Loading from './Loading.js';
import ImagesList from './ImagesList';
import SearchPhotos from './SearchPhotos';

class Explorer extends Component {

    state = {
      tag: 'recent'
    };

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <SearchPhotos tag={this.state.tag} />
    </View>
    );
  }
}

export default Explorer;
