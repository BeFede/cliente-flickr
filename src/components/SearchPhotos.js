
import React, { Component } from 'react';
import { ScrollView, Text, View, TextInput, Button } from 'react-native';
import axios from 'axios';
import PhotoDetail from './PhotoDetail';
import Loading from './Loading.js';
import ImagesList from './ImagesList';

class SearchPhotos extends Component {

    state = {
      photoset: null,
    };

  componentWillMount() {
    axios.get('https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=ebb2c0e717ba643d98ac4c252cd7497b&format=json&nojsoncallback=1&tags=' + this.props.tag)
      .then(response => this.setState({ photos: response.data.photos.photo }));
  }


  render() {

    if (!this.state.photos) {
			return (
          <Loading />
				);
    }

    return (

      <View style={{ flex: 1, flexDirection: 'column' }}>
        <ImagesList images={this.state.photos} />
    </View>

    );


  }
}

export default SearchPhotos;
